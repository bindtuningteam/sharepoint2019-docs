### Installing the theme on a site collection

1. Right-click the **Installer.ps1** file and select **Run with PowerShell** (run as Administrator);
2. Select **option 4 - Install SharePoint PnP PowerShell *2019* Cmdlets**;

	![1-pnp-install.png](../../images/install-pnp-2019.png)

3. Insert **A** or **Y** to allow for the installation of the **SharePoint PnP PowerShell *2019* Cmdlets**.

4. Select **option 1 or 2** to **install and apply** the theme to a corresponding site collection;

	![install-theme-site-collection.png](../../images/install-theme-site-collection.png)
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to <strong>Change the Look</strong> gallery and select from Company themes the theme.</p>
	
5. Insert the site collection URL where the theme should be applied;

	<p class="alert alert-success">We will automatically deploy the <strong><em>.sppkg</em></strong> package to your <strong>tenant app catalog</strong>. Instructions for <strong>further deployments of the same theme on other site collections</strong> can be found on the section below.</p>


6. Login with your SharePoint credentials;

	![add-credentials.png](../../images/add-credentials.png)

7. After inputting your credentials, the process will begin automatically. 

	![completed-process.png](../../images/completed-process.png)

Theme installed on the Site! ✅ 

---
### Applying the theme to a site collection

If you already have the theme available on your **tenant app catalog**, you can proceed to **apply it** to any SharePoint Site collection.

1. If you're not running form the previous section, right-click the **Installer.ps1** file and select **Run with PowerShell**;
2. Select **option 3 or 4**;

	![install-theme-site-collection.png](../../images/install-theme-site-collection.png)
	
	<p class="alert alert-success">Full color experience applies theme colors to Modern Page elements. To apply the theme colors, you must go to "Change the Look" gallery and select from Company themes the theme.</p>
	
3. Insert the site collection URL where the theme should be applied;

4.  Login with your SharePoint credentials;

	![add-credentials.png](../../images/add-credentials.png)

5. After inputting your credentials, the process will begin automatically. 

	![completed-process.png](../../images/completed-process.png)


Theme applied to the Site! ✅ 