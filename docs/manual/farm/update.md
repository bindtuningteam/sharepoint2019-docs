### Manual update

Before upgrading your theme's version, you will need to request the new version at <a href="https://bindtuning.com/">BindTuning</a> and follow the steps on the <a href="../../bindtuning/update" target="_blank">next link</a>.

<p class="alert alert-warning"><strong>ALWAYS</strong> make a backup of all of your theme assets, including CSS files, master pages, page layouts, etc. Upgrading the theme will remove all custom changes you have applied previously.</p> 

**Uninstall the theme**

To upgrade your theme version, you need to first completely remove the theme from your website. 
You can find the instructions for uninstalling a theme manually or using the One-Click tool here:

- <a href="../uninstall">One-Click Uninstall</a>

- <a href="../uninstall">Manual Uninstall</a>


**Install the theme again**

After completely removing the theme you can move on to installing the new version.

You can find the instructions for installing a theme manually or using the One-Click tool here: 

- <a href="../one-click installation">One-Click Installation</a>

- <a href="../manual installation">Manual Installation</a> 

Version upgraded! ✅

---

### One-Click update

1. Unzip the **.zip** file 
2. Open the "FarmSolution" folder;
3. Inside the "FarmSolution" folder, click twice on the **Setup.exe** file, to open the One-click installer;
4.  Click **Next**;

 	<p class="alert alert-success">The installer will now check if the theme can be upgraded. Once is done you will see a success message. If any of the requirements fail, click on <strong>Abort</strong>, fix the failed requirements and re-run the Installer.</p>

6.  Click **Next**; 

7.  Select **Upgrade** and click **Next**; 

	<p class="alert alert-success">An <strong>Upgrade</strong> option will only appear if the setup package has a greater version than the one installed.</p>

8.  Check if your theme appears on the list and click **Next**;

	<p class="alert alert-success">The theme will only appear if its activated on your site.</p>

9.  Your theme is now being upgraded - once is done you will see a success message! 🕐  

After the upgrade, click **Close** to close the One-click tool. You can also click **Next** to check the log.

Version upgraded! ✅