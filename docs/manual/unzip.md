The themes are provided in 3 different packaging all following Microsoft guidelines:

For **Classic Experience**: 

 - ***Farm Solution***, folder for Farm environments
 - ***Sandbox Solution***, for Sandbox environments.

For **Modern Experience**: 

 - ***Modern Solution***, Modern SharePoint.

After unzipping your Theme package, you will find three folders, a **Farm Solution**, **Sandbox Solution** and **Modern Solution**. 
Depending on the SharePoint configuration you may install all the packages.

![zip.png](../images/zip.png)


-------------

**Farm Solution** (Classic SharePoint) 📁

── res 📁

└── Eula.rtf <br>
└── *yourthemename.SP2019*.wsp<br>
└── logo.png<br>
└── start.png<br>

── Install.*yourthemenamePackage*.ps1<br>
── Setup.exe<br>
── Setup.exe.config<br>

-------------

**Sandbox Solution** (Classic SharePoint) 📁

── *yourthemename*.SP2019.wsp


-------------
**Modern Solution** (Modern SharePoint) 📁

── *yourthemename*.json<br>
── *yourthemename*.spcolor<br>
── *yourthemename*.spfx.sppkg<br>
── Installer.ps1<br>

---------
